#include "../include/bmp.h"
#include "../include/image.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#define BISIZE 40 
#define BIPLANES 1
#define BIBITCOUNT 24
#define BFTYPE 0x4D42

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

static int64_t padding(uint64_t width){
    return 4-((width)*3)%4;
}

static enum read_status read_header(FILE* in,struct bmp_header* head){
    if(feof(in))
        {return READ_INVALID_SIGNATURE;}
    fread(head, sizeof(struct bmp_header), 1, in);
    if( ferror( in ) )      {
         perror( "Read error" );
        return READ_INVALID_HEADER;
    }
    return  READ_OK;   
}

enum read_status from_bmp( FILE* in, struct image* const img ){
    struct bmp_header head;
    enum read_status readStatus;
    readStatus=read_header(in, &head);
    if(readStatus!=READ_OK){
    return readStatus;}
    *img=image_create_and_read(in, head.biWidth, head.biHeight);   
    if(is_image_empty(img)){
        
    return READ_INVALID_BITS;}
    
    return READ_OK;
}


static struct bmp_header header_create( uint64_t width, uint64_t height){
    struct bmp_header header = {
            .bfType = BFTYPE,
            .bfileSize = (sizeof(struct bmp_header)
                          + height* width * sizeof(struct pixel)
                          + height* (padding(width))),
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BISIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = BIPLANES
            ,
            .biBitCount = BIBITCOUNT,
            .biSizeImage = height * width * sizeof(struct pixel) + padding(width)*height,
        
    };
    return header;
}

static enum write_status write_header(FILE* out,struct bmp_header header){
    errno = 0;
    int64_t wasWrote=0;
    wasWrote=fwrite(&header, sizeof(struct bmp_header), 1, out);
    if(wasWrote==0){
        return WRITE_ERROR;
    }
    return WRITE_OK;
}
static enum write_status write_image1(FILE* out,struct image const* img){
    const uint8_t filler=0;
    for(uint32_t i=0; i< img->height; i++) {
        errno = 0;
        int64_t wasWrote=0;
        wasWrote=fwrite(&(img->data[i*img->width]),sizeof(struct pixel), img->width, out);
        if(wasWrote==0){
            return WRITE_ERROR;
        }
        for(uint32_t j=0; j< padding(img->width); j++){
        wasWrote=fwrite(&filler, 1, 1, out);
        if(wasWrote==0){
            return WRITE_ERROR;
         }
        }
    }
    return WRITE_OK;
}
enum write_status to_bmp( FILE* out, struct image const* img ) {
    
    struct bmp_header header = header_create(img->width,img->height);
    enum write_status writeStatus;
    writeStatus=write_header(out,header);
    if(writeStatus!=WRITE_OK)
        return writeStatus;
    writeStatus=write_image1(out, img);
    if(writeStatus!=WRITE_OK)
        return writeStatus;
    return WRITE_OK;
}


