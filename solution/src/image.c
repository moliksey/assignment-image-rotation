#include "../include/image.h"
#include <stdlib.h>

struct image;

static int64_t padding(uint64_t width){
    return 4-((width)*3)%4;
}

void destruct_image(struct image* const img){
    free(img->data);
}

 struct image image_create_and_read(FILE* in, uint64_t width, uint64_t height){
    struct image img={ width, height,  malloc(sizeof(struct pixel)*width*height)};
    
    if(feof(in))
        {
            destruct_image(&img);
            img.data=NULL;
            return img;
        }
    for(uint64_t i=0; i< img.height; i++) {
       
       fread(&(img.data[i*img.width]), sizeof(struct pixel),img.width, in);
       if( ferror( in ) )      {
         perror( "Read error" );
         destruct_image(&img);
         img.data=NULL;
         return img;
      }
       fseek( in, padding(img.width), SEEK_CUR);
    }
    return img;
}

void rewrite_pixel(const struct image* source, uint64_t i, uint64_t j,struct image* out){
        out->data[(out->width-1)+j*out->width-i]=source->data[j+i*(source->width)];
}

int8_t is_image_empty(struct image* const img){
return img->data==NULL;
}

struct image create_image_like_this(struct image source){
    struct image outImage = {source.height, source.width, (struct pixel*)(malloc(sizeof(struct pixel)*source.width*source.height))};
    return outImage;
}
