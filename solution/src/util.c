#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include <errno.h>
#include <stdlib.h>

enum write_status open_and_write_image(const char * fname, struct image const* img){
    enum write_status writeStatus;
    errno = 0;	
    FILE *outputfile = fopen(fname, "wb");
    if (outputfile) {
        writeStatus=to_bmp(outputfile, img);
        if(writeStatus!=0)
            return writeStatus;
        fclose(outputfile);
    }
    else {
        perror("fopen() ");
        return OPENOUT_ERROR;
    } 
    return WRITE_OK;
}

enum read_status open_and_read_image(const char * fname, struct image* const img){
    enum read_status readStatus;
    errno = 0;	
    FILE *inputfile = fopen(fname/*input.bmp"*/, "rb");
    if (inputfile) {
        readStatus=from_bmp(inputfile, img);
        if(readStatus!=0)
            return readStatus;
        fclose(inputfile);
    }
    else {
        perror("fopen() ");
        return OPENIN_ERROR;
    } 
    return READ_OK;
}
