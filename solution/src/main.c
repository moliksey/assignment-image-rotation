#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include "../include/util.h"
#include <errno.h>
#include <stdlib.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv; 
  
    struct image inputimg={0};
    struct image outputimg={0};
    enum read_status readStatus;
    enum write_status writeStatus;

    readStatus=open_and_read_image(argv[1], &inputimg);
    if(readStatus!=READ_OK)
    {       
        return readStatus;
    }
    outputimg = rotate(inputimg);
    writeStatus=open_and_write_image(argv[2], &outputimg);
    if(writeStatus!=WRITE_OK)
    {   
        return writeStatus;
    }
    destruct_image(&inputimg);
    destruct_image(&outputimg);
    return 0;
}
