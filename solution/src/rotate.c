#include "../include/image.h"
#include "../include/rotate.h"
#include <stdlib.h>

struct image rotate( struct image const source ) {
    struct image outImage =create_image_like_this(source);
    //struct image outImage = {source.height, source.width, (struct pixel*)(malloc(sizeof(struct pixel)*source.width*source.height))};
    for (int64_t i =0 ; i < source.height ; i++){
        for (uint64_t j = 0; j < source.width; j++) {
            rewrite_pixel(&source, i, j, &outImage);
        }
    }
    return outImage;
}
