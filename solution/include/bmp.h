#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H
#include "image.h"
#include <stdio.h>


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    OPENIN_ERROR
    
};

enum read_status from_bmp( FILE* in, struct image* const img );


enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    OPENOUT_ERROR
    
};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif 
