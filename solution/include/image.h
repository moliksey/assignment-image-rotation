#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <stdint.h>
#include <stdio.h>


struct pixel { uint8_t b, g, r; }__attribute__ ((__packed__));

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};
void destruct_image(struct image* const img);

struct image image_create_and_read(FILE* in , uint64_t width, uint64_t height);
int8_t is_image_empty(struct image* const img);
struct image create_image_like_this(struct image const source);
void rewrite_pixel(const struct image* source, uint64_t i, uint64_t j,struct image* out);
#endif
