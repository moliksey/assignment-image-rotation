#ifndef ASSIGNMENT_IMAGE_ROTATION_UTIL_H
#define ASSIGNMENT_IMAGE_ROTATION_UTIL_H
#include "../include/bmp.h"

enum write_status open_and_write_image(const char * fname, struct image const* img);
enum read_status open_and_read_image(const char * fname, struct image* const img);
#endif
